# Welcome to GitMate 2

The new version of GitMate - written in django!

## What is GitMate?

GitMate automates the code review process on your projects hosted on our
[supported platforms](#supported-platforms) to speed up the development. GitMate provides static
code analysis powered by coala and many more useful plugins specially tailored for your need.

Can't find a plugin you need? [Report an issue](https://gitlab.com/gitmate/open-source/gitmate-2/issues)
or [create you own](Developers/Writing_Plugin)!

## Supported Platforms

- GitHub
- GitLab(Experimental)
- BitBucket(Comming Soon)
